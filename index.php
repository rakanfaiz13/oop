<?php
    require_once 'animal.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    echo "<h3>Result :</h3>";

    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->get_name() . "<br>"; // "shaun"
    echo "Legs : " . $sheep->get_legs() . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br><br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->get_name() . "<br>"; // "buduk"
    echo "Legs : " . $kodok->get_legs() . "<br>"; // 4
    echo "Cold Blooded : " . $kodok->get_cold_blooded() . "<br>"; // "no"
    echo "Jump : ";
    $kodok->jump(); // "hop hop"
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");

    echo "Name : " . $sungokong->get_name() . "<br>"; // "kera sakti"
    echo "Legs : " . $sungokong->get_legs() . "<br>"; // 2
    echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>"; // "no"
    echo "Yell : ";
    $sungokong->yell(); // "Auooo"
?>